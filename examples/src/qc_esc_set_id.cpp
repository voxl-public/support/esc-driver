/****************************************************************************
 * Copyright (c) 2017 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name The Linux Foundation nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY 
 * THIS LICENSE.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * In addition Supplemental Terms apply.  See the SUPPLEMENTAL file.
 *
 ****************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "qc_esc_packet.h"

#include "platform.c"

#define VERSION "1.0"

using namespace std;

int main(int argc, char * argv[])
{
  char * dev    = (char*)"/dev/ttyUSB0";
  int baud_rate = 250000;
  
  debug_print("qc_esc_set_id version %s build date %s\n",VERSION,__DATE__);
  debug_print("Usage: qc_esc_set_id <device path> <baud_rate> <esc id>\n");
  
  if (argc < 4)
  {
    debug_print("need 3 arguments: <device path>, <baud rate>, <esc id>\n");
    return -1;
  }

  dev       = argv[1];
  baud_rate = atoi(argv[2]);
  int id    = atoi(argv[3]);
  
  if ((id<0) || (id>7))
  {
    debug_print("bad esc id : %d\n",id);
    return -1;
  }
  
  uint8_t buf[128];
  
  //generate the packet packet based on ID
  int packet_length = qc_esc_create_set_id_packet(id,buf,sizeof(buf));
  if (packet_length < 1)
  {
    debug_print("could not generate ESC packet\n");
    return -1;
  }
  
  debug_print("Sending ESC set id %d command: \r\n",id);

  debug_print("HEX: ");
  for (int ii=0; ii<packet_length; ii++)
    debug_print("0x%02X ",buf[ii]);
  
  debug_print("\r\nDEC: ");
  for (int ii=0; ii<packet_length; ii++)
    debug_print("%d ",buf[ii]);
  
  debug_print("\r\n");

  if (com_port_connect(dev,baud_rate))
  {
    debug_print("could not connect to device\n");
    return -1;
  }
  
  if (com_port_write((char*)buf,packet_length) != packet_length)
  {
    debug_print("could not write chars\n");
    return -1;
  }
  
  //wait for the data to be written before disconnecting
  sleep_us(10000);
  com_port_disconnect();
  
  debug_print("Success\n");

  return 0;
}
