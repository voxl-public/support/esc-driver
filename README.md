# Qualcomm ESC Examples
#### *- Version 1.0 -*

This folder contains C/C++ examples for communicating with Qualcomm-supported electronic speed controllers (ESCs).

## C/C++ Protocol Examples
This directory contains several examples focused on demonstrating how to
interface with Qualcomm ESC firmware. To compile these examples, change
directories to ```examples/``` and run ```make```. This requires that you
have make installed on your system and is most easily done on a Unix-style
OS such as Linux or Mac OSX.

A successful compilation will generate three outputs in the ```examples/```
directory as described below:
* qc_esc_reset
  * Usage: qc_esc_reset <device path> <baudrate> <esc id>
  * Description: this demonstrates how to send a reset command to an ESC with a specified ID.
* qc_esc_set_id
  * Usage: qc_esc_reset <device path> <baud rate> <esc id>
  * Description: this demonstrates how to set the software ID of an ESC. Note that this will attempt to set the ID of all receiving ESCs, so this is ill-suited for a 4-in-1 ESC where IDs are set in hardware.
* qc_esc_test
  * **Warning**: this test can spin motors. Remove propellers and ensure a safe operating environment before running this test.
  * Usage: qc_esc_test <test mode> <device path> <baud rate>
    * Test type 0: detect ESCs based on response to control packets
    * Test type 1: detect ESCs based on version request packets
    * Test type 2: spin the motors at 30% power
    * Test type 3: spin the motors at 4000 RPM using closed loop control
  * Description: This example demonstrates basic ESC functionality, including power/PWM-based control and RPM-based control. This example also demonstrates how request, receive and interprent feedback data from ESCs.
  
## Compiling for Custom Targets
Most of the code is platform-independent, however there is functionality that may need to be 
implemented when porting to a custom target. File platform.c contains those platform-dependent functions
* sleep_us : sleep for some number of microseconds
* debug_print, error_print : custom print functions
* com_port_connect, com_port_disconnect, com_port_read, com_port_write, com_port_set_baud_rate : functions for accessing the serial port